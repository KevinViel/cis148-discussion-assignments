/*

   Name:  Kevin Viel
   Date:  2018-10-17

   This application will demonstrate the creation of a collection of objects.
   There will be user input to create individual widget objects that contain
   a name field and a price.

   Modified: Kevin Viel
             2018-10-31  I updated the program to include the Scanner requirement (that I overlooked).


   Team member with branch:

*/

import java.util.Scanner ;

public class WidgetFactory {

    public static void main ( String[] args ) {
        Widget[] widgets = new Widget[ Widget.getMaxWidgets()] ;
        /*
           - Set up a Scanner object to read user input
           - Use a loop to
                 - prompt the user to input a widget name and price
                 - store values in a new widget
                 - add the widget to a widget array
                 - update the widget count
           - You can only create up to the maximum number of widgets allowed
           - Display the collection of widgets and the average price

        */

        Scanner scnr = new Scanner( System.in ) ;

        String userWidget ;
        Float  userPrice  ;
        int    userCnt    ;

        userCnt = 0 ;

        do {
            ++userCnt ;

            System.out.println( "Enter a Widget name or \"quit\" to end:" ) ;
            userWidget = scnr.next() ;

            if ( ! userWidget.equals( "quit" )) {

                System.out.println("Enter a price:") ;
                userPrice = scnr.nextFloat() ;
                Widget.updateCount() ;

                widgets[userCnt - 1] = new Widget
                        (userWidget
                                , userPrice
                        ) ;

            }
            else{ userCnt = Widget.getMaxWidgets() + 1 ; }  // To end

        } while ( userCnt < Widget.getMaxWidgets()  ) ;

        /*
        Widget widget  = new Widget( ) ;
        Widget widget2 = new Widget( "Gadget"
                                   , 10.5
                                   ) ;

        widgets[0] = widget  ;
        Widget.updateCount() ;
        widgets[1] = widget2 ;
        Widget.updateCount() ;
        widgets[2] = new Widget() ;
        Widget.updateCount() ;
        widgets[3] = new Widget( "Whatsit"
                               , 5.75
                               ) ;
        Widget.updateCount() ;
        */

        /*
        for ( Widget w : widgets ) {
            System.out.println( w ) ;
        }
        */

        for ( int i = 0 ; i < Widget.getCount() ; i++ ) {
            System.out.println( widgets[ i ] ) ;
        }

    }
}
