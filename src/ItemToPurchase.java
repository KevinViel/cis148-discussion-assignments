/*
   Name:  Kevin Viel
   Date:  2018-11-18

   This application will demonstrate default constructors and Public member methods (mutators & accessors).

   7.21 Ch 7 Warm up: Online shopping cart (Java)

   Build the ItemToPurchase class with the following specifications:

   Private fields
   String itemName - Initialized in default constructor to "none"
   int itemPrice - Initialized in default constructor to 0
   int itemQuantity - Initialized in default constructor to 0
   Default constructor
   Public member methods (mutators & accessors)
   setName() & getName() (2 pts)
   setPrice() & getPrice() (2 pts)
   setQuantity() & getQuantity() (2 pts)


   Modified by:        Kevin Viel
   Modification Date:  2018-11-28

   7.22 Ch 7 Program: Online Shopping Cart Completed

   (1) Extend the ItemToPurchase class per the following specifications:

   Private fields
   string itemDescription - Initialized in default constructor to "none"
   Parameterized constructor to assign item name, item description, item price, and item quantity (default values of 0). (1 pt)
   Public member methods
   setDescription() mutator & getDescription() accessor (2 pts)
   printItemCost() - Outputs the item name followed by the quantity, price, and subtotal
   printItemDescription() - Outputs the item name and description

*/

public class ItemToPurchase {

    private String itemName  ;
    private int    itemPrice ;
    private int    itemQuantity ;

    private String itemDescription ;

    /* Default constructor
         Initialized in default constructor to "none"
         Initialized in default constructor to 0
         Initialized in default constructor to 0
    */
    public void ItemInitVal() {
        itemName         = "none" ;
        itemPrice        = 0 ;
        itemQuantity     = 0 ;

        itemDescription  = "none" ;
    }

    // Argument based Constructor
    public void ItemInitValArg
       ( String itemName
       , int    itemPrice
       , int    itemQuantity
       , String itemDescription
       ) {
        this.itemName        = itemName ;
        this.itemPrice       = itemPrice ;
        this.itemQuantity    = itemQuantity ;
        this.itemDescription = itemDescription ;
    }

    /* Public member methods (mutators & accessors) */
    /* itemName */
    public void setName( String userName ) {
        itemName = userName ;
    }

    public String getName() {
        return itemName ;
    }

    /* itemPrice */
    public void setPrice( int userPrice ) {
        itemPrice = userPrice ;
    }

    public int getPrice() {
        return itemPrice ;
    }

    /* itemQuantity */
    public void setQuantity ( int userQuantity ) {
        itemQuantity = userQuantity ;
    }

    public int getQuantity () {
        return itemQuantity ;
    }

    /* itemDescription */
    public void setDescription( String userDescription ) {
        itemDescription = userDescription ;
    }

    public String getDescription() {
        return itemDescription ;
    }



    public void print() {
        System.out.println( itemName
                          + " "
                          + itemQuantity
                          + " @ $"
                          + itemPrice
                          + " = $"
                          + ( itemQuantity * itemPrice )
                          ) ;
    }

    /*    printItemCost() - Outputs the item name followed by the quantity, price, and subtotal */
    public void printItemCost() {
        System.out.println( itemName
                + " "
                + itemQuantity
                + " @ $"
                + itemPrice
                + " = $"
                + ( itemQuantity * itemPrice )
                ) ;
    }

    /* printItemDescription() - Outputs the item name and description */
    public void printItemDescription() {
        System.out.println( itemName
                + ": "
                + itemDescription
                ) ;
    }

}