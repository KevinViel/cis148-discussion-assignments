System.out.println( /*
   Name:  Kevin Viel
   Date:  2018-11-28

   This application will TBD

   7.22 Ch 7 Program: Online Shopping Cart Completed

Build the ShoppingCart class with the following specifications. Note: Some can be method stubs (empty methods) initially, to be completed in later steps.

Private fields
String customerName - Initialized in default constructor to "none"
String currentDate - Initialized in default constructor to "January 1, 2016"
ArrayList cartItems
Default constructor
Parameterized constructor which takes the customer name and date as parameters (1 pt)
Public member methods
getCustomerName() accessor (1 pt)
getDate() accessor (1 pt)
addItem()
Adds an item to cartItems array. Has parameter ItemToPurchase. Does not return anything.
removeItem()
Removes item from cartItems array. Has a string (an item's name) parameter. Does not return anything.
If item name cannot be found, output this message: Item not found in cart. Nothing removed.
modifyItem()
Modifies an item's description, price, and/or quantity. Has parameter ItemToPurchase. Does not return anything.
If item can be found (by name) in cart, check if parameter has default values for description, price, and quantity. If not, modify item in cart.
If item cannot be found (by name) in cart, output this message: Item not found in cart. Nothing modified.
getNumItemsInCart() (2 pts)
Returns quantity of all items in cart. Has no parameters.
getCostOfCart() (2 pts)
Determines and returns the total cost of items in cart. Has no parameters.
printTotal()
Outputs total of objects in cart.
If cart is empty, output this message: SHOPPING CART IS EMPTY
printDescriptions()
Outputs each item's description.

Ex. of printTotal() output:

John Doe's Shopping Cart - February 1, 2016
Number of Items: 8

Nike Romaleos 2 @ $189 = $378
Chocolate Chips 5 @ $3 = $15
Powerbeats 2 Headphones 1 @ $128 = $128

Total: $521

Ex. of printDescriptions() output:

John Doe's Shopping Cart - February 1, 2016

Item Descriptions
Nike Romaleos: Volt color, Weightlifting shoes
Chocolate Chips: Semi-sweet
Powerbeats 2 Headphones: Bluetooth headphones


*/


import java.util.Scanner ;

public class ShoppingCart {

    private String customerName ;
    private String currentDate ;
    private String[] cartItems         = new String[ 999 ] ;
    private String[] cartDescriptions  = new String[ 999 ] ;
    private double[] cartPrices        = new double[ 999 ] ;
    private int[]    cartQuantities    = new int[ 999 ] ;

    private int cartItemsSize ;

    cartItemsSize = 0 ;

    /* Default constructor */
    public void InitVal() {
        customerName    = "none" ;
        currentDate     = "January 1, 2016" ;
    }

    // Argument based Constructor
    public void InitValArg
    ( String customerName
    , String currentDate
    ) {
        this.customerName = customerName ;
        this.currentDate  = currentDate ;
    }


    /* Public member methods */
    public String getCustomerName() {
        return customerName ;
    }

    public String getDate() {
        return currentDate ;
    }

    public void addItem( String ItemToPurchase ) {
        /*  Adds an item to cartItems array. Has parameter ItemToPurchase. Does not return anything. */
        cartItems[ cartItemsSize + 1 ] = ItemToPurchase ;
    }

    public void removeItem( String itemToRemove ) {

        /* Removes item from cartItems array. Has a string (an item's name) parameter. Does not return anything.
           If item name cannot be found, output this message: Item not found in cart. Nothing removed.
        */

        boolean found ;
        int i ;

        found = false ;

        for ( i = 0 ; i < cartItemsSize ; ++i ) {

            if ( found ) {
                cartItems       [ i - 1 ] = cartItems       [ i ] ;
                cartDescriptions[ i - 1 ] = cartDescriptions[ i ] ;
                cartPrices      [ i - 1 ] = cartPrices      [ i ] ;
                cartQuantities  [ i - 1 ] = cartQuantities  [ i ] ;

            }
             else if ( cartItems[ i ] == itemToRemove ) {
                found = true;
            }
        } // END OF for loop

        if ( ! found ) {
            System.out.println( "Item not found in cart. Nothing removed." ) ;
        } else{ cartItemsSize +-1 ; }

    }  // END OF removeItem


    public void modifyItem( String modifyName
                          , String modifyDescription
                          , int modifyPrice
                          , int modifyQuantity
                          ) {
        /* Modifies an item's description, price, and/or quantity. Has parameter ItemToPurchase. Does not return anything.
           If item can be found (by name) in cart, check if parameter has default values for description, price, and quantity. If not, modify item in cart.
           If item cannot be found (by name) in cart, output this message: Item not found in cart. Nothing modified.
        */

        boolean found ;
        int i ;

        found = false ;

        for ( i = 0 ; i < cartItemsSize ; ++i ) {

            if ( cartItems[ i ] == modifyName ) {
                found = true;

                if ( cartDescriptions[ i ] == "none" ) {
                    cartDescriptions[ i ] = modifyDescription ;
                }

                if ( cartPrices[ i ] == 0 ) {
                    cartPrices[ i ] = modifyPrice ;
                }
                if ( cartQuantities[ i ] == 0 ) {
                    cartQuantities[ i ] = modifyQuantity ;
                }

                // First occurrence
                i = cartItemsSize + 1 ;

            } // END OF cartDescriptions[ i ] == "none"
        } // END OF for loop

        if ( ! found ) {
            System.out.println( "Item not found in cart. Nothing modified." ) ;
        }
    } // END OF modifyItem

    public int getNumItemsInCart() {
        /* Returns quantity of all items in cart. Has no parameters. */
        int sum ;
        int i ;

        sum = 0 ;

        for ( i = 0 ; i < cartItemsSize ; ++i ) {
            sum ++ cartQuantities[ i ] ;
        }

        return sum ;
    }

    public int getCostOfCart() {
        /* Determines and returns the total cost of items in cart. Has no parameters. */
        double total ;
        int i ;

        total = 0 ;

        for ( i = 0 ; i < cartItemsSize ; ++i ) {
            total ++ ( cartPrices[ i ] * cartQuantities[ i ]) ;
        }

        return total ;

    }

    public void printTotal() {
        if ( cartItemsSize > 0 ) {

            System.out.println( customerName
                              + "'s Shopping care - "
                              + currentDate
                              ) ;
            System.out.println( "Number of Items: "
                              + getNumItemsInCart()
                              ) ;
            System.out.println( "" ) ;


            for ( int i = 0 ; i < cartItemsSize ; ++i ) {

                System.out.println( cartItems[ i ]
                                  + " "
                                  + cartQuantities[ i ]
                                  + " @ $"
                                  + cartPrices[ i ]
                                  + " = $"
                                  + ( cartQuantities[ i ] * cartPrices[ i ] )
                                  ) ;

            } // END OF for loop

            System.out.println( "Total: $"
                              + getCostOfCart()
                              ) ;

        }
         else {
            System.out.println( "SHOPPING CART IS EMPTY" ) ;
         }


    }






}