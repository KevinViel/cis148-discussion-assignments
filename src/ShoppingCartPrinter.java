/*
   Name:  Kevin Viel
   Date:  2018-11-18

   This application will TBD

   7.21 Ch 7 Warm up: Online shopping cart (Java)

   (2) In main(), prompt the user for two items and create two objects of the ItemToPurchase class. Before prompting for the second item, call scnr.nextLine(); to allow the user to input a new string. (2 pts)
   (3) Add the costs of the two items together and output the total cost. (2 pts)

Ex:

Item 1
Enter the item name:
Chocolate Chips
Enter the item price:
3
Enter the item quantity:
1

Item 2
Enter the item name:
Bottled Water
Enter the item price:
1
Enter the item quantity:
10


Ex:

TOTAL COST
Chocolate Chips 1 @ $3 = $3
Bottled Water 10 @ $1 = $10

Total: $13
*/


import java.util.Scanner ;

public class ShoppingCartPrinter {

    public static void main( String[] args ) {

        Scanner scnr = new Scanner( System.in ) ;

        /* (2) In main(), prompt the user for two items and create two objects of the ItemToPurchase class.
               Before prompting for the second item, call scnr.nextLine(); to allow the user to input a new string. (2 pts)

        */
        ItemToPurchase userSet1 = new ItemToPurchase() ;
        ItemToPurchase userSet2 = new ItemToPurchase() ;

        System.out.println( "Item 1" ) ;
        System.out.println( "Enter the item name:" ) ;
        userSet1.setName( scnr.nextLine()) ;
        System.out.println( "Enter the item price:" ) ;
        userSet1.setPrice( scnr.nextInt()) ;
        System.out.println( "Enter the item quantity:" ) ;
        userSet1.setQuantity( scnr.nextInt()) ;

        // Before prompting for the second item, call scnr.nextLine(); to allow the user to input a new string.
        scnr.nextLine() ;

        System.out.println( "\nItem 2" ) ;
        System.out.println( "Enter the item name:" ) ;
        userSet2.setName( scnr.nextLine()) ;
        System.out.println( "Enter the item price:" ) ;
        userSet2.setPrice( scnr.nextInt()) ;
        System.out.println( "Enter the item quantity:" ) ;
        userSet2.setQuantity( scnr.nextInt()) ;


        System.out.println( "\nTOTAL COST" ) ;
        userSet1.print() ;
        userSet2.print() ;

        System.out.println( "" ) ;

        /* (3) Add the costs of the two items together and output the total cost. (2 pts) */
        System.out.println( "Total: $"
                          + ( userSet1.getQuantity() * userSet1.getPrice()
                            + userSet2.getQuantity() * userSet2.getPrice()
                            )
                          ) ;

    } // END OF main

}
