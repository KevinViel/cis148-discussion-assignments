/* Name: Kevin Viel
   Date: 02Sep2018

   CIS148 Introduction to Java
   1.7 Module 1 Attendance: Basic output with variables (Java)
*/

/* A variable like userNum can store a value like an integer. Extend the given program to print userNum values as indicated. */


import java.util.Scanner;

public class OutputWithVars {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in) ;
        int userNum   ;
        int userNum_2 ;
        int userNum_3 ;
        int userNum2  ;
        int sum       ;
        int prod      ;

        System.out.println("Enter integer:") ;
        userNum = scnr.nextInt() ;

        /* 1) Output the user's input. (2 pts) */
        System.out.println( "You entered: "
                + userNum
        ) ;

        /* (2) Extend to output the input squared and cubed. Hint: Compute squared as userNum * userNum. (2 pts) */
        userNum_2 = userNum   * userNum ;
        userNum_3 = userNum_2 * userNum ;

        System.out.println( userNum
                + " squared is "
                + userNum_2
        ) ;
        System.out.println( "And "
                + userNum
                + " cubed is "
                + userNum_3
                + "!!"
        ) ;

        /* (3) Extend to get a second user input into userNum2. Output sum and product. (1 pt) */
        System.out.println("Enter another integer:") ;
        userNum2 = scnr.nextInt() ;  // KRV: Output does not include userNum2.

        sum  = userNum + userNum2 ;
        prod = userNum * userNum2 ;

        System.out.println( userNum
                + " + "
                + userNum2
                + " is "
                + sum
        ) ;
        System.out.println( userNum
                + " * "
                + userNum2
                + " is "
                + prod
        ) ;

    }
}


/*
Enter integer:
You entered: 3
3 squared is 9
And 3 cubed is 27!!
Enter another integer:
3 + 6 is 9
3 * 6 is 18
*/